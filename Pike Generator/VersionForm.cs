﻿using Microsoft.Win32;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Resources;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pike_Generator
{
    public partial class VersionForm : Form
    {

        int clicked = 1;

        public VersionForm()
        {
            InitializeComponent();

            
         


            pikeLauncherBtn.BackColor = ColorTranslator.FromHtml("#000e26");
            pikeGeneratorBtn.BackColor = ColorTranslator.FromHtml("#002361");
            highliter.Location = new Point(
            pikeGeneratorBtn.Location.X,
            highliter.Location.Y);

            try
            {
                htmlBrowser.DocumentText = Properties.Resources.ChangelogGenerator;
            }
            catch (Exception ex)
            {
                String fehlermeldung = ex.ToString();
            }

            clicked = 1;





        }

        private void lollipopButton1_Click(object sender, EventArgs e)
        {
            if(clicked != 1)
            {
                pikeLauncherBtn.BackColor = ColorTranslator.FromHtml("#000e26");
                pikeGeneratorBtn.BackColor = ColorTranslator.FromHtml("#002361");
                highliter.Location = new Point(
                pikeGeneratorBtn.Location.X,
                highliter.Location.Y);

                try
                {
                    htmlBrowser.DocumentText = Properties.Resources.ChangelogGenerator;
                }
                catch (Exception ex)
                {
                    String fehlermeldung = ex.ToString();
                }

                clicked = 1;
            
            }
            
        }

        private void lollipopButton2_Click(object sender, EventArgs e)
        {

            if(clicked != 2)
            {
                pikeLauncherBtn.BackColor = ColorTranslator.FromHtml("#002361");
                pikeGeneratorBtn.BackColor = ColorTranslator.FromHtml("#000e26");
                highliter.Location = new Point(
                pikeLauncherBtn.Location.X,
                highliter.Location.Y);

                try
                {
                    htmlBrowser.DocumentText = Properties.Resources.ChangelogLauncher;
                }catch(Exception ex){
                    String fehlermeldung = ex.ToString();
                }
                
                clicked = 2;
            }
            
        }

        private void lizenzBtn_Click(object sender, EventArgs e)
        {





        }

        private void rechnerId_Click(object sender, EventArgs e)
        {
           
            MessageBox.Show("Rufen Sie die Rechner ID über Strg + V ab.");
        }

        private void lizenzantrag_Click(object sender, EventArgs e)
        {

        }
    }
}
