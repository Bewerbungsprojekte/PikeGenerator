﻿using System.Collections.Generic;

public class Config
{
    public string baseurl { get; set; }
    public string version { get; set; }
    public string adminpassword { get; set; }
    public List<string> apps { get; set; }
    public bool enterprisebrowser { get; set; }
    public int scale { get; set; }
    public bool enableBack { get; set; }
    public bool enableHome { get; set; }
    public bool enableJavascript { get; set; }
    public bool displayNavigationbar { get; set; }
    public bool pinchtozoom { get; set; }
    public bool pinchtozoombuttons { get; set; }
    public string displaymode { get; set; }

}