﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pike_Generator
{
    public partial class Package : Form
    {

        int currentClick = 0;
        public Package()
        {
            InitializeComponent();

            packageName.Enter += new EventHandler(package_Clicked);
            packageName.Leave += new EventHandler(package_Leave);
        }

        private void package_Clicked(object sender, EventArgs e)
        {
            packageName.Text = "";
        }

        private void package_Leave(object sender, EventArgs e)
        {
            if (packageName.Text.Equals(""))
            {
                packageName.Text = "Packagename";
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {
            if (currentClick == 1)
            {
                pictureBox1.Image = Properties.Resources._1Anleitung;
                currentClick--;
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

            if(currentClick == 0)
            {
                pictureBox1.Image = Properties.Resources._2Anleitung;
                currentClick++;

            }



        }

        private void lollipopButton2_Click(object sender, EventArgs e)
        {
            Package.ActiveForm.Dispose();

        }

        private void lollipopButton1_Click(object sender, EventArgs e)
        {
            if (!packageName.Text.Equals(""))
            {
                AppListe.Instance.addAppToList(packageName.Text);
                Package.ActiveForm.Dispose();
            }
        }
    }
}
