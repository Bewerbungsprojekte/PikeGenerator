﻿using Pike_Generator.JSONData;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using System.Linq;
using System.Reflection;
using System.Collections;

namespace Pike_Generator
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();



                    lollipopButton1.Enabled = true;
                    lollipopButton1.Text = "Config erzeugen";



            passwortTb.Enter += new EventHandler(passwortTb_Clicked);
            passwortTb.Leave += new EventHandler(passwortTb_Leave);

            urlTb.Enter += new EventHandler(urlTb_Clicked);
            urlTb.Leave += new EventHandler(urlTb_Leave);

            scaleTb.Enter += new EventHandler(scaleTb_Clicked);
            scaleTb.Leave += new EventHandler(scaleTb_Leave);
            
            this.FormBorderStyle = FormBorderStyle.FixedSingle;

            showAnwendungen.setBackColor("#e9cb48");
            showEinstellungen.setBackColor("#e9cb48");

            versionLabel.Text = Application.ProductVersion;


                ApplicationState.Instance.Aktiviert = false;

            ToolTip tooltip = new ToolTip();
            tooltip.AutoPopDelay = 9000;
            tooltip.InitialDelay = 1;
            tooltip.ReshowDelay = 500;
            tooltip.ShowAlways = true;

            tooltip.SetToolTip(pinchInfoPb, "Aktiviert / Deaktiviert die Pinch-To-Zoom Geste");
            tooltip.SetToolTip(homeInfoPb, "Zeigt im Browser ein Control an um auf die Startseite zugelangen");
            tooltip.SetToolTip(backInfoPb, "Ermöglicht es über die Zurücktaste auf die vorherige Seite zu gelangen");
            tooltip.SetToolTip(javascriptInfoPb, "Aktiviert / Deaktiviert JavaScript in der Anwendung");
            tooltip.SetToolTip(zoomInfoPb, "Blendet die Zoom Controls an/aus wenn Pinch-To-Zoom aktiv ist");
            tooltip.SetToolTip(navigationInfoPb, "Blendet die obere Navigationsleiste ein/aus");
            
        }

        private void scaleTb_Clicked(object sender, EventArgs e)
        {
            if (scaleTb.Text.Equals("Skalierung"))
            {
                scaleTb.Text = "";
            }
        }
        private void scaleTb_Leave(object sender, EventArgs e)
        {
            if (scaleTb.Text.Equals(""))
            {
                scaleTb.Text = "Skalierung";
            }
        }


        private void passwortTb_Clicked(object sender, EventArgs e)
        {
            if (passwortTb.Text.Equals("Passwort"))
            {
                passwortTb.Text = "";
            }

        }
        private void passwortTb_Leave(object sender, EventArgs e)
        {
            if (passwortTb.Text.Equals("")) {
                passwortTb.Text = "Passwort";
            }
        }

        private void urlTb_Clicked(object sender, EventArgs e)
        {
            if (urlTb.Text.Equals("URL"))
            {
                urlTb.Text = "";
            }
        }
        private void urlTb_Leave(object sender, EventArgs e)
        {
            if (urlTb.Text.Equals(""))
            {
                urlTb.Text = "URL";
            }
        }
        
        private void resetControlls()
        {

            showAnwendungen.Location = new Point(
                this.showAnwendungen.Location.X,
                this.showAnwendungen.Location.Y - einstellungenContainer.Height);

            anwendungenContainer.Location = new Point(
                this.anwendungenContainer.Location.X,
                this.anwendungenContainer.Location.Y - einstellungenContainer.Height);

            showBrowser.Location = new Point(
                this.showBrowser.Location.X,
                this.showBrowser.Location.Y - einstellungenContainer.Height);

            browserContainer.Location = new Point(
                this.browserContainer.Location.X,
                this.browserContainer.Location.Y - einstellungenContainer.Height);

            showAnwendungen.setBackColor("#e9cb48");
            showEinstellungen.setBackColor("#e9cb48");
            showBrowser.setBackColor("#e9cb48");

        }
        
        private void showAnwendungen_Click(object sender, EventArgs e)
        {
            if (einstellungenContainer.Visible)
            {
                resetControlls();

            }

            if (!anwendungenContainer.Visible)
            {
                collapseAll();
                anwendungenContainer.Visible = true;
                moveAnwendungenControll();
                moveBrowserControll();
            }
            else
            {
                resetBrowser();
                collapseAll();
            }
        }

        private void resetBrowser()
        {
            showBrowser.Location = new Point(
                            this.showBrowser.Location.X,
                            this.showBrowser.Location.Y - anwendungenContainer.Height);

            browserContainer.Location = new Point(
                this.browserContainer.Location.X,
                this.browserContainer.Location.Y - anwendungenContainer.Height);
        }

        private void showEinstellungen_Click(object sender, EventArgs e)
        {
            if (anwendungenContainer.Visible)
            {
                resetBrowser();
            }

            if (!einstellungenContainer.Visible)
            {
                collapseAll();
                einstellungenContainer.Visible = true;
                moveAnwendungenControll();
                moveBrowserControll();
                
            }
            else
            {
                resetControlls();
                collapseAll();

            }
        }
        
        private void moveAnwendungenControll()
        {

            if (einstellungenContainer.Visible)
            {
                showAnwendungen.Location = new Point(
                this.showAnwendungen.Location.X,
                this.showAnwendungen.Location.Y + einstellungenContainer.Height);

                anwendungenContainer.Location = new Point(
                this.anwendungenContainer.Location.X,
                this.anwendungenContainer.Location.Y + einstellungenContainer.Height);


            }


            
        }


        private void moveBrowserControll()
        {
            if (anwendungenContainer.Visible)
            {
                showBrowser.Location = new Point(
                this.showBrowser.Location.X,
                this.showBrowser.Location.Y + anwendungenContainer.Height);

                browserContainer.Location = new Point(
                this.browserContainer.Location.X,
                this.browserContainer.Location.Y + anwendungenContainer.Height);
            }

            if (einstellungenContainer.Visible)
            {
                showBrowser.Location = new Point(
                this.showBrowser.Location.X,
                this.showBrowser.Location.Y + einstellungenContainer.Height);

                browserContainer.Location = new Point(
                this.browserContainer.Location.X,
                this.browserContainer.Location.Y + einstellungenContainer.Height);
            }
        }


        private void collapseAll()
        {
            anwendungenContainer.Visible = false;
            browserContainer.Visible = false;
            einstellungenContainer.Visible = false;
        }

        private void lollipopButton1_Click(object sender, EventArgs e)
        {

            if (!passwortTb.Text.Equals("Passwort"))
            {
                Config config = new Config();
                config.adminpassword = passwortTb.Text;
                config.baseurl = urlTb.Text;
                config.version = majorTb.Text + "." + minorTb.Text;

                if (browserSw.CheckState == CheckState.Checked)
                {
                    config.enterprisebrowser = true;
                }
                else
                {
                    config.enterprisebrowser = false;
                }

                var displaymode = groupBox1.Controls.OfType<RadioButton>().FirstOrDefault(n => n.Checked);

                config.displaymode = displaymode.Text;
                
                config.enableBack = backButtonCb.Checked;
                config.enableHome = homeButtonCb.Checked;
                config.displayNavigationbar = navigationBarCb.Checked;
                config.enableJavascript = javaScriptCb.Checked;
                config.pinchtozoom = pinchCb.Checked;
                config.pinchtozoombuttons = zoomButtonsCb.Checked;

                config.apps = AppListe.Instance.apps;

                if (!scaleTb.Text.Equals("Skalierung"))
                {
                    config.scale = Convert.ToInt32(scaleTb.Text);
                }
                else
                {
                    config.scale = 100;
                }


                var json = new JavaScriptSerializer().Serialize(config);

                String test = json.ToString();

                String pfad = "";

                Verschlüsselung verschlüsselung = new Verschlüsselung();

                if (folderBrowserDialog1.ShowDialog() == DialogResult.OK)
                {
                    pfad = folderBrowserDialog1.SelectedPath;
                    System.IO.File.WriteAllText(pfad + @"\config.pike", verschlüsselung.Encrypt(json.ToString()));
                }
            }
            else
            {
                MessageBox.Show("Bitte geben Sie ein Passwort zum verlassen des Gesicherten Modus an.");
            }




        }
        
        private void addAppBtn_Click(object sender, EventArgs e)
        {
            AppListe.Instance.listBox = selectedAppsLB;

            Package addApp = new Package();
            addApp.ShowDialog();
        }

        public void changeSelectedApps(List<string> selectedApps)
        {
            selectedAppsLB.DataSource = selectedApps;
        }

        private void removeAppBtn_Click(object sender, EventArgs e)
        {
            if(selectedAppsLB.SelectedIndex == -1)
            {
                MessageBox.Show("Datei auswählen!");
            }
            else
            {
                AppListe.Instance.removeAppFromList(selectedAppsLB.SelectedIndex);
                selectedAppsLB.Items.RemoveAt(selectedAppsLB.SelectedIndex);
            }
        }
        
        private void majorTb_TextChanged(object sender, EventArgs e)
        {
            string currentText = majorTb.Text;

            try
            {

                if (majorTb.Text != "")
                {
                    var nummer = Convert.ToDouble(majorTb.Text);
                }
            }
            catch (Exception ex)
            {
                majorTb.Text = currentText.Remove(currentText.Length - 1);
            }
        }

        private void minorTb_TextChanged(object sender, EventArgs e)
        {
            string currentText = minorTb.Text;

            try
            {
                if (minorTb.Text != "")
                {
                    var nummer = Convert.ToDouble(minorTb.Text);
                }
            }
            catch (Exception ex)
            {
                minorTb.Text = currentText.Remove(currentText.Length - 1);
            }
        }

        private void adminPasswordEt_hover(object sender, EventArgs e)
        {

        }

        private void adminPasswordEt_leave(object sender, EventArgs e)
        {

        }

        private void adminPasswordEt_hover(object sender, MouseEventArgs e)
        {

        }

        private void versionLabel_Click(object sender, EventArgs e)
        {
            VersionForm versionForm = new VersionForm();
            versionForm.Show();
        }

        private void Form1_Enter(object sender, EventArgs e)
        {

                    lollipopButton1.Enabled = true;
                    lollipopButton1.Text = "Config erzeugen";

        }

        private void showBrowser_Click(object sender, EventArgs e)
        {
            if (einstellungenContainer.Visible)
            {
                resetControlls();

            }

            if (anwendungenContainer.Visible)
            {
                resetBrowser();
            }

            if (!browserContainer.Visible)
            {
                collapseAll();
                browserContainer.Visible = true;
                moveBrowserControll();
            }
            else
            {
                collapseAll();
            }

        }

        private void pinchCb_CheckedChanged(object sender, EventArgs e)
        {
            if (pinchCb.Checked)
            {
                zoomButtonsCb.Visible = true;
                zoomInfoPb.Visible = true;
            }
            else
            {
                zoomButtonsCb.Visible = false;
                zoomInfoPb.Visible = false;
                zoomButtonsCb.Checked = false;
            }
        }

        private void lollipopButton2_Click(object sender, EventArgs e)
        {
            String pfad = "";

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                pfad = openFileDialog1.FileName;
                String data = System.IO.File.ReadAllText(pfad);

                Verschlüsselung verschlüsselung = new Verschlüsselung();
                String file = verschlüsselung.Decrypt(data);
                


                string[] daten;
                

                

                JavaScriptSerializer j = new JavaScriptSerializer();

                try
                {
                  Object obj =  j.DeserializeObject(file);
                   loadConfig(obj);
                  

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }

                //String[] a = j.Deserialize(file, typeof(String[]));

                //loadConfig(a);

            }



        }

        private void loadConfig(object data)
        {

            var col = data as IEnumerable;


            if(col is ICollection)
            {

                foreach (object o in col)
                {

                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty("Key");
                    String name = (String)(pi.GetValue(o, null));
                    String value = "";


                    if (name.Equals("apps"))
                    {
                        System.Reflection.PropertyInfo appsValues = o.GetType().GetProperty("Value");
                        Object[] apps = (object[])(appsValues.GetValue(o, null));

                        foreach(object app in apps)
                        {
                            value = value + app + ",";
                        }

                        writeData(name, value);

                    }
                    else
                    {
                        System.Reflection.PropertyInfo pi2 = o.GetType().GetProperty("Value");
                       
                        var typecheck = (pi2.GetValue(o, null));

                        if (typecheck.GetType() == typeof(String))
                        {

                            value = (String)(pi2.GetValue(o, null));
                        }
                        else
                        {
                            
                            value = typecheck.ToString();
                        }
                        
                        writeData(name, value);
                    }

                    

                    
                }

            }

            


        }

        private void writeData(string key, string value)
        {
            switch (key)
            {
                case "baseurl":
                    urlTb.Text = value;
                    break;
                case "adminpassword":
                    passwortTb.Text = value;
                    break;
                case "apps":
                    value = value.Remove(value.Length - 1, 1);
                    string[] apps = value.Split(',');

                    foreach(string i in apps)
                    {
                        AppListe.Instance.addAppToList(i);
                    }

                    break;

                case "version":
                    break;
                case "enterprisebrowser":
                    if (value.Equals("True"))
                    {
                        browserSw.Checked = true;
                    }
                    else
                    {
                        browserSw.Checked = false;
                    }
                    break;
                case "scale":
                    scaleTb.Text = value;
                    break;
                case "enableBack":
                    if (value.Equals("True"))
                    {
                            backButtonCb.Checked = true;
                    }
                    else
                    {
                        backButtonCb.Checked = false;
                    }
                    break;
                case "enableHome":
                    if (value.Equals("True"))
                    {
                        homeButtonCb.Checked = true;
                    }
                    else
                    {
                        homeButtonCb.Checked = false;
                    }
                    break;
                case "enableJavascript":
                    if (value.Equals("True"))
                    {
                        javaScriptCb.Checked = true;
                    }
                    else
                    {
                        javaScriptCb.Checked = false;
                    }
                    break;
                case "displayNavigationbar":
                    if (value.Equals("True"))
                    {
                        navigationBarCb.Checked = true;
                    }
                    else
                    {
                        navigationBarCb.Checked = false;
                    }
                    break;
                case "pinchtozoom":
                    if (value.Equals("True"))
                    {
                        pinchCb.Checked = true;
                        zoomButtonsCb.Visible = true;
                        zoomInfoPb.Visible = true;

                    }
                    else
                    {
                        pinchCb.Checked = false;
                        zoomButtonsCb.Visible = false;
                        zoomInfoPb.Visible = false;
                    }
                    break;
                case "pinchtozoombuttons":
                    if (value.Equals("True"))
                    {
                        zoomButtonsCb.Checked = true;
                    }
                    else
                    {
                        zoomButtonsCb.Checked = false;
                    }
                    break;
                case "displaymode":
                    if (value.Equals("Landscape"))
                    {
                        radioButton2.Checked = true;
                    }
                    else if(value.Equals("Portrait"))
                    {
                        radioButton1.Checked = false;
                    }else if (value.Equals("Auto"))
                    {
                        radioButton3.Checked = false;
                    }
                    break;

            }
        }
    }
}
