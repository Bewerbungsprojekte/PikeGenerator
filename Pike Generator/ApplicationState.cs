﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pike_Generator
{
    class ApplicationState
    {

        private static readonly ApplicationState instance = new ApplicationState();

        public Boolean Aktiviert { get; set; }


        private ApplicationState() { }

        public static ApplicationState Instance
        {
            get
            {
                return instance;
            }
        }


    }
}
