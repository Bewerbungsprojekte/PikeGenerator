﻿namespace Pike_Generator
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.einstellungenContainer = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.lollipopLabel5 = new LollipopLabel();
            this.minorTb = new LollipopTextBox();
            this.majorTb = new LollipopTextBox();
            this.lollipopLabel4 = new LollipopLabel();
            this.passwortTb = new LollipopTextBox();
            this.anwendungenContainer = new System.Windows.Forms.Panel();
            this.selectedAppsLB = new System.Windows.Forms.ListBox();
            this.removeAppBtn = new LollipopButton();
            this.addAppBtn = new LollipopButton();
            this.settingsPanel = new System.Windows.Forms.Panel();
            this.lollipopButton2 = new LollipopButton();
            this.browserContainer = new System.Windows.Forms.Panel();
            this.zoomInfoPb = new System.Windows.Forms.PictureBox();
            this.javascriptInfoPb = new System.Windows.Forms.PictureBox();
            this.navigationInfoPb = new System.Windows.Forms.PictureBox();
            this.pinchInfoPb = new System.Windows.Forms.PictureBox();
            this.backInfoPb = new System.Windows.Forms.PictureBox();
            this.homeInfoPb = new System.Windows.Forms.PictureBox();
            this.zoomButtonsCb = new System.Windows.Forms.CheckBox();
            this.pinchCb = new System.Windows.Forms.CheckBox();
            this.javaScriptCb = new System.Windows.Forms.CheckBox();
            this.navigationBarCb = new System.Windows.Forms.CheckBox();
            this.backButtonCb = new System.Windows.Forms.CheckBox();
            this.homeButtonCb = new System.Windows.Forms.CheckBox();
            this.scaleTb = new LollipopTextBox();
            this.urlTb = new LollipopTextBox();
            this.lollipopLabel6 = new LollipopLabel();
            this.lollipopLabel8 = new LollipopLabel();
            this.browserSw = new LollipopToggle();
            this.showBrowser = new LollipopFlatButton();
            this.lollipopButton1 = new LollipopButton();
            this.showEinstellungen = new LollipopFlatButton();
            this.showAnwendungen = new LollipopFlatButton();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.versionLabel = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.einstellungenContainer.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.anwendungenContainer.SuspendLayout();
            this.settingsPanel.SuspendLayout();
            this.browserContainer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomInfoPb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.javascriptInfoPb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationInfoPb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pinchInfoPb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.backInfoPb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.homeInfoPb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // einstellungenContainer
            // 
            this.einstellungenContainer.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.einstellungenContainer.BackColor = System.Drawing.Color.White;
            this.einstellungenContainer.Controls.Add(this.groupBox1);
            this.einstellungenContainer.Controls.Add(this.lollipopLabel5);
            this.einstellungenContainer.Controls.Add(this.minorTb);
            this.einstellungenContainer.Controls.Add(this.majorTb);
            this.einstellungenContainer.Controls.Add(this.lollipopLabel4);
            this.einstellungenContainer.Controls.Add(this.passwortTb);
            this.einstellungenContainer.Location = new System.Drawing.Point(35, 55);
            this.einstellungenContainer.Name = "einstellungenContainer";
            this.einstellungenContainer.Size = new System.Drawing.Size(304, 144);
            this.einstellungenContainer.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Location = new System.Drawing.Point(6, 97);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(287, 44);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Displaymodus";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(202, 19);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(47, 17);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.Text = "Auto";
            this.radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(107, 19);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(78, 17);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "Landscape";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(30, 19);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(58, 17);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Portrait";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // lollipopLabel5
            // 
            this.lollipopLabel5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lollipopLabel5.AutoSize = true;
            this.lollipopLabel5.BackColor = System.Drawing.Color.Transparent;
            this.lollipopLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lollipopLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lollipopLabel5.Location = new System.Drawing.Point(177, 52);
            this.lollipopLabel5.Name = "lollipopLabel5";
            this.lollipopLabel5.Size = new System.Drawing.Size(12, 17);
            this.lollipopLabel5.TabIndex = 4;
            this.lollipopLabel5.Text = ".";
            // 
            // minorTb
            // 
            this.minorTb.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.minorTb.FocusedColor = "#508ef5";
            this.minorTb.FontColor = "64; 64; 64";
            this.minorTb.IsEnabled = true;
            this.minorTb.Location = new System.Drawing.Point(199, 52);
            this.minorTb.MaxLength = 32767;
            this.minorTb.Multiline = false;
            this.minorTb.Name = "minorTb";
            this.minorTb.ReadOnly = false;
            this.minorTb.Size = new System.Drawing.Size(29, 24);
            this.minorTb.TabIndex = 3;
            this.minorTb.Text = "0";
            this.minorTb.TextAlignment = System.Windows.Forms.HorizontalAlignment.Left;
            this.minorTb.UseSystemPasswordChar = false;
            this.minorTb.TextChanged += new System.EventHandler(this.minorTb_TextChanged);
            // 
            // majorTb
            // 
            this.majorTb.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.majorTb.FocusedColor = "#508ef5";
            this.majorTb.FontColor = "64; 64; 64";
            this.majorTb.IsEnabled = true;
            this.majorTb.Location = new System.Drawing.Point(143, 52);
            this.majorTb.MaxLength = 32767;
            this.majorTb.Multiline = false;
            this.majorTb.Name = "majorTb";
            this.majorTb.ReadOnly = false;
            this.majorTb.Size = new System.Drawing.Size(28, 24);
            this.majorTb.TabIndex = 2;
            this.majorTb.Text = "1";
            this.majorTb.TextAlignment = System.Windows.Forms.HorizontalAlignment.Left;
            this.majorTb.UseSystemPasswordChar = false;
            this.majorTb.TextChanged += new System.EventHandler(this.majorTb_TextChanged);
            // 
            // lollipopLabel4
            // 
            this.lollipopLabel4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lollipopLabel4.AutoSize = true;
            this.lollipopLabel4.BackColor = System.Drawing.Color.Transparent;
            this.lollipopLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lollipopLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lollipopLabel4.Location = new System.Drawing.Point(3, 53);
            this.lollipopLabel4.Name = "lollipopLabel4";
            this.lollipopLabel4.Size = new System.Drawing.Size(104, 17);
            this.lollipopLabel4.TabIndex = 1;
            this.lollipopLabel4.Text = "Config Version:";
            // 
            // passwortTb
            // 
            this.passwortTb.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.passwortTb.FocusedColor = "#508ef5";
            this.passwortTb.FontColor = "#999999";
            this.passwortTb.IsEnabled = true;
            this.passwortTb.Location = new System.Drawing.Point(6, 22);
            this.passwortTb.MaxLength = 32767;
            this.passwortTb.Multiline = false;
            this.passwortTb.Name = "passwortTb";
            this.passwortTb.ReadOnly = false;
            this.passwortTb.Size = new System.Drawing.Size(287, 24);
            this.passwortTb.TabIndex = 0;
            this.passwortTb.Text = "Passwort";
            this.passwortTb.TextAlignment = System.Windows.Forms.HorizontalAlignment.Left;
            this.passwortTb.UseSystemPasswordChar = false;
            // 
            // anwendungenContainer
            // 
            this.anwendungenContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.anwendungenContainer.BackColor = System.Drawing.Color.White;
            this.anwendungenContainer.Controls.Add(this.selectedAppsLB);
            this.anwendungenContainer.Controls.Add(this.removeAppBtn);
            this.anwendungenContainer.Controls.Add(this.addAppBtn);
            this.anwendungenContainer.Location = new System.Drawing.Point(35, 245);
            this.anwendungenContainer.Name = "anwendungenContainer";
            this.anwendungenContainer.Size = new System.Drawing.Size(304, 186);
            this.anwendungenContainer.TabIndex = 6;
            this.anwendungenContainer.Visible = false;
            // 
            // selectedAppsLB
            // 
            this.selectedAppsLB.FormattingEnabled = true;
            this.selectedAppsLB.Location = new System.Drawing.Point(6, 46);
            this.selectedAppsLB.Name = "selectedAppsLB";
            this.selectedAppsLB.Size = new System.Drawing.Size(287, 134);
            this.selectedAppsLB.TabIndex = 7;
            // 
            // removeAppBtn
            // 
            this.removeAppBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(75)))), ((int)(((byte)(131)))));
            this.removeAppBtn.BGColor = "#508ef5";
            this.removeAppBtn.FontColor = "#ffffff";
            this.removeAppBtn.Location = new System.Drawing.Point(208, 7);
            this.removeAppBtn.Name = "removeAppBtn";
            this.removeAppBtn.Size = new System.Drawing.Size(85, 33);
            this.removeAppBtn.TabIndex = 6;
            this.removeAppBtn.Text = "Remove App";
            this.removeAppBtn.Click += new System.EventHandler(this.removeAppBtn_Click);
            // 
            // addAppBtn
            // 
            this.addAppBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(66)))), ((int)(((byte)(75)))), ((int)(((byte)(131)))));
            this.addAppBtn.BGColor = "#508ef5";
            this.addAppBtn.FontColor = "#ffffff";
            this.addAppBtn.Location = new System.Drawing.Point(6, 7);
            this.addAppBtn.Name = "addAppBtn";
            this.addAppBtn.Size = new System.Drawing.Size(85, 33);
            this.addAppBtn.TabIndex = 5;
            this.addAppBtn.Text = "Add App";
            this.addAppBtn.Click += new System.EventHandler(this.addAppBtn_Click);
            // 
            // settingsPanel
            // 
            this.settingsPanel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.settingsPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(14)))), ((int)(((byte)(38)))));
            this.settingsPanel.Controls.Add(this.lollipopButton2);
            this.settingsPanel.Controls.Add(this.browserContainer);
            this.settingsPanel.Controls.Add(this.anwendungenContainer);
            this.settingsPanel.Controls.Add(this.showBrowser);
            this.settingsPanel.Controls.Add(this.lollipopButton1);
            this.settingsPanel.Controls.Add(this.showEinstellungen);
            this.settingsPanel.Controls.Add(this.einstellungenContainer);
            this.settingsPanel.Controls.Add(this.showAnwendungen);
            this.settingsPanel.Location = new System.Drawing.Point(475, 12);
            this.settingsPanel.Name = "settingsPanel";
            this.settingsPanel.Size = new System.Drawing.Size(408, 658);
            this.settingsPanel.TabIndex = 7;
            // 
            // lollipopButton2
            // 
            this.lollipopButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lollipopButton2.BackColor = System.Drawing.Color.SteelBlue;
            this.lollipopButton2.BGColor = "46; 183; 46";
            this.lollipopButton2.FontColor = "#ffffff";
            this.lollipopButton2.Location = new System.Drawing.Point(35, 604);
            this.lollipopButton2.Name = "lollipopButton2";
            this.lollipopButton2.Size = new System.Drawing.Size(84, 41);
            this.lollipopButton2.TabIndex = 12;
            this.lollipopButton2.Text = "Datei import";
            this.lollipopButton2.Click += new System.EventHandler(this.lollipopButton2_Click);
            // 
            // browserContainer
            // 
            this.browserContainer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.browserContainer.BackColor = System.Drawing.Color.White;
            this.browserContainer.Controls.Add(this.zoomInfoPb);
            this.browserContainer.Controls.Add(this.javascriptInfoPb);
            this.browserContainer.Controls.Add(this.navigationInfoPb);
            this.browserContainer.Controls.Add(this.pinchInfoPb);
            this.browserContainer.Controls.Add(this.backInfoPb);
            this.browserContainer.Controls.Add(this.homeInfoPb);
            this.browserContainer.Controls.Add(this.zoomButtonsCb);
            this.browserContainer.Controls.Add(this.pinchCb);
            this.browserContainer.Controls.Add(this.javaScriptCb);
            this.browserContainer.Controls.Add(this.navigationBarCb);
            this.browserContainer.Controls.Add(this.backButtonCb);
            this.browserContainer.Controls.Add(this.homeButtonCb);
            this.browserContainer.Controls.Add(this.scaleTb);
            this.browserContainer.Controls.Add(this.urlTb);
            this.browserContainer.Controls.Add(this.lollipopLabel6);
            this.browserContainer.Controls.Add(this.lollipopLabel8);
            this.browserContainer.Controls.Add(this.browserSw);
            this.browserContainer.Location = new System.Drawing.Point(35, 292);
            this.browserContainer.Name = "browserContainer";
            this.browserContainer.Size = new System.Drawing.Size(304, 263);
            this.browserContainer.TabIndex = 11;
            this.browserContainer.Visible = false;
            // 
            // zoomInfoPb
            // 
            this.zoomInfoPb.Cursor = System.Windows.Forms.Cursors.Help;
            this.zoomInfoPb.Image = global::Pike_Generator.Properties.Resources.help;
            this.zoomInfoPb.Location = new System.Drawing.Point(276, 161);
            this.zoomInfoPb.Name = "zoomInfoPb";
            this.zoomInfoPb.Size = new System.Drawing.Size(17, 17);
            this.zoomInfoPb.TabIndex = 20;
            this.zoomInfoPb.TabStop = false;
            this.zoomInfoPb.Visible = false;
            // 
            // javascriptInfoPb
            // 
            this.javascriptInfoPb.Cursor = System.Windows.Forms.Cursors.Help;
            this.javascriptInfoPb.Image = global::Pike_Generator.Properties.Resources.help;
            this.javascriptInfoPb.Location = new System.Drawing.Point(276, 138);
            this.javascriptInfoPb.Name = "javascriptInfoPb";
            this.javascriptInfoPb.Size = new System.Drawing.Size(17, 17);
            this.javascriptInfoPb.TabIndex = 19;
            this.javascriptInfoPb.TabStop = false;
            // 
            // navigationInfoPb
            // 
            this.navigationInfoPb.Cursor = System.Windows.Forms.Cursors.Help;
            this.navigationInfoPb.Image = global::Pike_Generator.Properties.Resources.help;
            this.navigationInfoPb.Location = new System.Drawing.Point(276, 115);
            this.navigationInfoPb.Name = "navigationInfoPb";
            this.navigationInfoPb.Size = new System.Drawing.Size(17, 17);
            this.navigationInfoPb.TabIndex = 18;
            this.navigationInfoPb.TabStop = false;
            // 
            // pinchInfoPb
            // 
            this.pinchInfoPb.Cursor = System.Windows.Forms.Cursors.Help;
            this.pinchInfoPb.Image = global::Pike_Generator.Properties.Resources.help;
            this.pinchInfoPb.Location = new System.Drawing.Point(116, 161);
            this.pinchInfoPb.Name = "pinchInfoPb";
            this.pinchInfoPb.Size = new System.Drawing.Size(17, 17);
            this.pinchInfoPb.TabIndex = 17;
            this.pinchInfoPb.TabStop = false;
            // 
            // backInfoPb
            // 
            this.backInfoPb.Cursor = System.Windows.Forms.Cursors.Help;
            this.backInfoPb.Image = global::Pike_Generator.Properties.Resources.help;
            this.backInfoPb.Location = new System.Drawing.Point(116, 138);
            this.backInfoPb.Name = "backInfoPb";
            this.backInfoPb.Size = new System.Drawing.Size(17, 17);
            this.backInfoPb.TabIndex = 16;
            this.backInfoPb.TabStop = false;
            // 
            // homeInfoPb
            // 
            this.homeInfoPb.Cursor = System.Windows.Forms.Cursors.Help;
            this.homeInfoPb.Image = global::Pike_Generator.Properties.Resources.help;
            this.homeInfoPb.Location = new System.Drawing.Point(116, 115);
            this.homeInfoPb.Name = "homeInfoPb";
            this.homeInfoPb.Size = new System.Drawing.Size(17, 17);
            this.homeInfoPb.TabIndex = 15;
            this.homeInfoPb.TabStop = false;
            // 
            // zoomButtonsCb
            // 
            this.zoomButtonsCb.AutoSize = true;
            this.zoomButtonsCb.Location = new System.Drawing.Point(174, 161);
            this.zoomButtonsCb.Name = "zoomButtonsCb";
            this.zoomButtonsCb.Size = new System.Drawing.Size(92, 17);
            this.zoomButtonsCb.TabIndex = 14;
            this.zoomButtonsCb.Text = "Zoom Buttons";
            this.zoomButtonsCb.UseVisualStyleBackColor = true;
            this.zoomButtonsCb.Visible = false;
            // 
            // pinchCb
            // 
            this.pinchCb.AutoSize = true;
            this.pinchCb.Location = new System.Drawing.Point(6, 161);
            this.pinchCb.Name = "pinchCb";
            this.pinchCb.Size = new System.Drawing.Size(99, 17);
            this.pinchCb.TabIndex = 13;
            this.pinchCb.Text = "Pinch-To-Zoom";
            this.pinchCb.UseVisualStyleBackColor = true;
            this.pinchCb.CheckedChanged += new System.EventHandler(this.pinchCb_CheckedChanged);
            // 
            // javaScriptCb
            // 
            this.javaScriptCb.AutoSize = true;
            this.javaScriptCb.Location = new System.Drawing.Point(174, 138);
            this.javaScriptCb.Name = "javaScriptCb";
            this.javaScriptCb.Size = new System.Drawing.Size(76, 17);
            this.javaScriptCb.TabIndex = 12;
            this.javaScriptCb.Text = "JavaScript";
            this.javaScriptCb.UseVisualStyleBackColor = true;
            // 
            // navigationBarCb
            // 
            this.navigationBarCb.AutoSize = true;
            this.navigationBarCb.Location = new System.Drawing.Point(174, 115);
            this.navigationBarCb.Name = "navigationBarCb";
            this.navigationBarCb.Size = new System.Drawing.Size(96, 17);
            this.navigationBarCb.TabIndex = 11;
            this.navigationBarCb.Text = "Navigation Bar";
            this.navigationBarCb.UseVisualStyleBackColor = true;
            // 
            // backButtonCb
            // 
            this.backButtonCb.AutoSize = true;
            this.backButtonCb.Location = new System.Drawing.Point(6, 138);
            this.backButtonCb.Name = "backButtonCb";
            this.backButtonCb.Size = new System.Drawing.Size(85, 17);
            this.backButtonCb.TabIndex = 10;
            this.backButtonCb.Text = "Back Button";
            this.backButtonCb.UseVisualStyleBackColor = true;
            // 
            // homeButtonCb
            // 
            this.homeButtonCb.AutoSize = true;
            this.homeButtonCb.Location = new System.Drawing.Point(6, 115);
            this.homeButtonCb.Name = "homeButtonCb";
            this.homeButtonCb.Size = new System.Drawing.Size(88, 17);
            this.homeButtonCb.TabIndex = 9;
            this.homeButtonCb.Text = "Home Button";
            this.homeButtonCb.UseVisualStyleBackColor = true;
            // 
            // scaleTb
            // 
            this.scaleTb.FocusedColor = "#508ef5";
            this.scaleTb.FontColor = "#999999";
            this.scaleTb.IsEnabled = true;
            this.scaleTb.Location = new System.Drawing.Point(6, 85);
            this.scaleTb.MaxLength = 32767;
            this.scaleTb.Multiline = false;
            this.scaleTb.Name = "scaleTb";
            this.scaleTb.ReadOnly = false;
            this.scaleTb.Size = new System.Drawing.Size(295, 24);
            this.scaleTb.TabIndex = 8;
            this.scaleTb.Text = "Skalierung";
            this.scaleTb.TextAlignment = System.Windows.Forms.HorizontalAlignment.Left;
            this.scaleTb.UseSystemPasswordChar = false;
            // 
            // urlTb
            // 
            this.urlTb.FocusedColor = "#508ef5";
            this.urlTb.FontColor = "#999999";
            this.urlTb.IsEnabled = true;
            this.urlTb.Location = new System.Drawing.Point(6, 55);
            this.urlTb.MaxLength = 32767;
            this.urlTb.Multiline = false;
            this.urlTb.Name = "urlTb";
            this.urlTb.ReadOnly = false;
            this.urlTb.Size = new System.Drawing.Size(295, 24);
            this.urlTb.TabIndex = 4;
            this.urlTb.Text = "URL";
            this.urlTb.TextAlignment = System.Windows.Forms.HorizontalAlignment.Left;
            this.urlTb.UseSystemPasswordChar = false;
            // 
            // lollipopLabel6
            // 
            this.lollipopLabel6.AutoSize = true;
            this.lollipopLabel6.BackColor = System.Drawing.Color.Transparent;
            this.lollipopLabel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lollipopLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lollipopLabel6.Location = new System.Drawing.Point(58, 17);
            this.lollipopLabel6.Name = "lollipopLabel6";
            this.lollipopLabel6.Size = new System.Drawing.Size(75, 17);
            this.lollipopLabel6.TabIndex = 3;
            this.lollipopLabel6.Text = "Deaktiviert";
            // 
            // lollipopLabel8
            // 
            this.lollipopLabel8.AutoSize = true;
            this.lollipopLabel8.BackColor = System.Drawing.Color.Transparent;
            this.lollipopLabel8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.lollipopLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lollipopLabel8.Location = new System.Drawing.Point(205, 17);
            this.lollipopLabel8.Name = "lollipopLabel8";
            this.lollipopLabel8.Size = new System.Drawing.Size(58, 17);
            this.lollipopLabel8.TabIndex = 2;
            this.lollipopLabel8.Text = "Aktiviert";
            // 
            // browserSw
            // 
            this.browserSw.AutoSize = true;
            this.browserSw.EllipseBorderColor = "#3b73d1";
            this.browserSw.EllipseColor = "#508ef5";
            this.browserSw.Location = new System.Drawing.Point(142, 17);
            this.browserSw.Name = "browserSw";
            this.browserSw.Size = new System.Drawing.Size(47, 19);
            this.browserSw.TabIndex = 1;
            this.browserSw.Text = "lollipopToggle1";
            this.browserSw.UseVisualStyleBackColor = true;
            // 
            // showBrowser
            // 
            this.showBrowser.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.showBrowser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(203)))), ((int)(((byte)(72)))));
            this.showBrowser.FontColor = "#508ef5";
            this.showBrowser.Location = new System.Drawing.Point(35, 251);
            this.showBrowser.Name = "showBrowser";
            this.showBrowser.Size = new System.Drawing.Size(304, 41);
            this.showBrowser.TabIndex = 10;
            this.showBrowser.Text = "Enterprise Browser";
            this.showBrowser.Click += new System.EventHandler(this.showBrowser_Click);
            // 
            // lollipopButton1
            // 
            this.lollipopButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lollipopButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(183)))), ((int)(((byte)(46)))));
            this.lollipopButton1.BGColor = "46; 183; 46";
            this.lollipopButton1.FontColor = "#ffffff";
            this.lollipopButton1.Location = new System.Drawing.Point(125, 604);
            this.lollipopButton1.Name = "lollipopButton1";
            this.lollipopButton1.Size = new System.Drawing.Size(214, 41);
            this.lollipopButton1.TabIndex = 0;
            this.lollipopButton1.Text = "Datei erstellen";
            this.lollipopButton1.Click += new System.EventHandler(this.lollipopButton1_Click);
            // 
            // showEinstellungen
            // 
            this.showEinstellungen.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.showEinstellungen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(203)))), ((int)(((byte)(72)))));
            this.showEinstellungen.FontColor = "#508ef5";
            this.showEinstellungen.Location = new System.Drawing.Point(35, 15);
            this.showEinstellungen.Name = "showEinstellungen";
            this.showEinstellungen.Size = new System.Drawing.Size(304, 41);
            this.showEinstellungen.TabIndex = 3;
            this.showEinstellungen.Text = "Einstellungen";
            this.showEinstellungen.Click += new System.EventHandler(this.showEinstellungen_Click);
            // 
            // showAnwendungen
            // 
            this.showAnwendungen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.showAnwendungen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(233)))), ((int)(((byte)(203)))), ((int)(((byte)(72)))));
            this.showAnwendungen.FontColor = "#508ef5";
            this.showAnwendungen.Location = new System.Drawing.Point(35, 205);
            this.showAnwendungen.Name = "showAnwendungen";
            this.showAnwendungen.Size = new System.Drawing.Size(304, 41);
            this.showAnwendungen.TabIndex = 1;
            this.showAnwendungen.Text = "Anwendungen";
            this.showAnwendungen.Click += new System.EventHandler(this.showAnwendungen_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Pike_Generator.Properties.Resources.enterprise_logo;
            this.pictureBox1.Location = new System.Drawing.Point(12, 120);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(455, 437);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.versionLabel.ForeColor = System.Drawing.Color.White;
            this.versionLabel.Location = new System.Drawing.Point(12, 656);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(35, 13);
            this.versionLabel.TabIndex = 9;
            this.versionLabel.Text = "label1";
            this.versionLabel.Click += new System.EventHandler(this.versionLabel_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(14)))), ((int)(((byte)(38)))));
            this.ClientSize = new System.Drawing.Size(895, 683);
            this.Controls.Add(this.versionLabel);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.settingsPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Pike Generator";
            this.Activated += new System.EventHandler(this.Form1_Enter);
            this.Enter += new System.EventHandler(this.Form1_Enter);
            this.einstellungenContainer.ResumeLayout(false);
            this.einstellungenContainer.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.anwendungenContainer.ResumeLayout(false);
            this.settingsPanel.ResumeLayout(false);
            this.browserContainer.ResumeLayout(false);
            this.browserContainer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.zoomInfoPb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.javascriptInfoPb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.navigationInfoPb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pinchInfoPb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.backInfoPb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.homeInfoPb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LollipopButton lollipopButton1;
        private LollipopFlatButton showAnwendungen;
        private LollipopFlatButton showEinstellungen;
        private LollipopTextBox passwortTb;
        private System.Windows.Forms.Panel einstellungenContainer;
        private System.Windows.Forms.Panel anwendungenContainer;
        private System.Windows.Forms.Panel settingsPanel;
        private System.Windows.Forms.ListBox selectedAppsLB;
        private LollipopButton removeAppBtn;
        private LollipopButton addAppBtn;
        private LollipopTextBox minorTb;
        private LollipopTextBox majorTb;
        private LollipopLabel lollipopLabel4;
        private LollipopLabel lollipopLabel5;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.Panel browserContainer;
        private System.Windows.Forms.CheckBox pinchCb;
        private System.Windows.Forms.CheckBox javaScriptCb;
        private System.Windows.Forms.CheckBox navigationBarCb;
        private System.Windows.Forms.CheckBox backButtonCb;
        private System.Windows.Forms.CheckBox homeButtonCb;
        private LollipopTextBox scaleTb;
        private LollipopTextBox urlTb;
        private LollipopLabel lollipopLabel6;
        private LollipopLabel lollipopLabel8;
        private LollipopToggle browserSw;
        private LollipopFlatButton showBrowser;
        private System.Windows.Forms.CheckBox zoomButtonsCb;
        private System.Windows.Forms.PictureBox zoomInfoPb;
        private System.Windows.Forms.PictureBox javascriptInfoPb;
        private System.Windows.Forms.PictureBox navigationInfoPb;
        private System.Windows.Forms.PictureBox pinchInfoPb;
        private System.Windows.Forms.PictureBox backInfoPb;
        private System.Windows.Forms.PictureBox homeInfoPb;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private LollipopButton lollipopButton2;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

