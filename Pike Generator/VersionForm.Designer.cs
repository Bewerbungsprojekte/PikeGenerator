﻿namespace Pike_Generator
{
    partial class VersionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.htmlBrowser = new System.Windows.Forms.WebBrowser();
            this.highliter = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.pikeLauncherBtn = new LollipopButton();
            this.pikeGeneratorBtn = new LollipopButton();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(97)))));
            this.panel1.Controls.Add(this.htmlBrowser);
            this.panel1.Location = new System.Drawing.Point(12, 59);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(291, 363);
            this.panel1.TabIndex = 2;
            // 
            // htmlBrowser
            // 
            this.htmlBrowser.Dock = System.Windows.Forms.DockStyle.Fill;
            this.htmlBrowser.Location = new System.Drawing.Point(0, 0);
            this.htmlBrowser.MinimumSize = new System.Drawing.Size(20, 20);
            this.htmlBrowser.Name = "htmlBrowser";
            this.htmlBrowser.Size = new System.Drawing.Size(291, 363);
            this.htmlBrowser.TabIndex = 0;
            // 
            // highliter
            // 
            this.highliter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(97)))));
            this.highliter.Location = new System.Drawing.Point(12, 48);
            this.highliter.Name = "highliter";
            this.highliter.Size = new System.Drawing.Size(143, 21);
            this.highliter.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Roboto Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(352, 59);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 29);
            this.label1.TabIndex = 4;
            this.label1.Text = "Pike Generator:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Roboto Light", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(589, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(165, 29);
            this.label2.TabIndex = 5;
            this.label2.Text = "Pike Launcher:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Roboto Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(373, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(141, 15);
            this.label3.TabIndex = 6;
            this.label3.Text = "Aktuelle Version: 1.0.0.0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Roboto Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(610, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 15);
            this.label4.TabIndex = 7;
            this.label4.Text = "Aktuelle Version: 1.0.0.0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Roboto Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(610, 127);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(165, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "Entwickelt von Julius Herold";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Roboto Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(373, 125);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(165, 15);
            this.label6.TabIndex = 9;
            this.label6.Text = "Entwickelt von Julius Herold";
            // 
            // pikeLauncherBtn
            // 
            this.pikeLauncherBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(14)))), ((int)(((byte)(38)))));
            this.pikeLauncherBtn.BGColor = "#508ef5";
            this.pikeLauncherBtn.FontColor = "#ffffff";
            this.pikeLauncherBtn.Location = new System.Drawing.Point(161, 12);
            this.pikeLauncherBtn.Name = "pikeLauncherBtn";
            this.pikeLauncherBtn.Size = new System.Drawing.Size(143, 41);
            this.pikeLauncherBtn.TabIndex = 1;
            this.pikeLauncherBtn.Text = "Pike Launcher";
            this.pikeLauncherBtn.Click += new System.EventHandler(this.lollipopButton2_Click);
            // 
            // pikeGeneratorBtn
            // 
            this.pikeGeneratorBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(35)))), ((int)(((byte)(97)))));
            this.pikeGeneratorBtn.BGColor = "#508ef5";
            this.pikeGeneratorBtn.FontColor = "#ffffff";
            this.pikeGeneratorBtn.Location = new System.Drawing.Point(12, 12);
            this.pikeGeneratorBtn.Name = "pikeGeneratorBtn";
            this.pikeGeneratorBtn.Size = new System.Drawing.Size(143, 41);
            this.pikeGeneratorBtn.TabIndex = 0;
            this.pikeGeneratorBtn.Text = "Pike Generator";
            this.pikeGeneratorBtn.Click += new System.EventHandler(this.lollipopButton1_Click);
            // 
            // VersionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(14)))), ((int)(((byte)(38)))));
            this.ClientSize = new System.Drawing.Size(840, 434);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.highliter);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pikeLauncherBtn);
            this.Controls.Add(this.pikeGeneratorBtn);
            this.Name = "VersionForm";
            this.Text = "VersionForm";
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private LollipopButton pikeGeneratorBtn;
        private LollipopButton pikeLauncherBtn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel highliter;
        private System.Windows.Forms.WebBrowser htmlBrowser;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}