﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pike_Generator
{

    

    class AppListe
    {
        public ListBox listBox;

        public List<string> apps = new List<string>();

        private static readonly AppListe instance = new AppListe();


        private AppListe() { }

        public static AppListe Instance
        {
            get
            {
                return instance;
            }
        }

        public void addAppToList(string Packagename)
        {
            apps.Add(Packagename);
            listBox.Items.Add(Packagename);
        }

        public void removeAppFromList(int position)
        {
            apps.RemoveAt(position);
        }

    }
}
